CREATE DATABASE app_food;
USE app_food;

CREATE TABLE `user` (
	user_id int AUTO_INCREMENT,
    full_name varchar(255),
    email varchar(255),
    `password` varchar(255),
    PRIMARY KEY (user_id)    
);

CREATE TABLE food (
	food_id int AUTO_INCREMENT,
    food_name varchar(255),
    image varchar(255),
    price float,
    `desc` varchar(255),
    type_id int,
    PRIMARY KEY (food_id)
);

CREATE TABLE `order` (
	user_id int,
    food_id int,
    amount int,
    `code` varchar(255),
    arr_sub_id varchar(255),
    PRIMARY KEY (user_id, food_id),
    FOREIGN KEY (user_id) REFERENCES `user`(user_id),
    FOREIGN KEY (food_id) REFERENCES food(food_id)
);

CREATE TABLE sub_food (
	sub_id int AUTO_INCREMENT,
    sub_name varchar(255),
    sub_price float,
    food_id int,
    PRIMARY KEY (sub_id),
    FOREIGN KEY (food_id) REFERENCES food(food_id)
);

CREATE TABLE food_type (
	type_id int AUTO_INCREMENT,
    type_name varchar(255),
    PRIMARY KEY (type_id)
);
/* add foreign key table `food` */
ALTER TABLE food
ADD FOREIGN KEY (type_id) REFERENCES food_type(type_id);

CREATE TABLE restaurant (
	res_id int AUTO_INCREMENT,
    res_name varchar(255),
    image varchar(255),
    `desc` varchar(255),
    PRIMARY KEY (res_id)
);

CREATE TABLE rate_res (
	user_id int,
    res_id int,
    amount int,
    date_rate datetime,
    PRIMARY KEY (user_id, res_id),
    FOREIGN KEY (user_id) REFERENCES `user`(user_id),
    FOREIGN KEY (res_id) REFERENCES restaurant(res_id)
);

CREATE TABLE like_res (
	user_id int,
    res_id int,
    date_like datetime,
    PRIMARY KEY (user_id, res_id),
    FOREIGN KEY (user_id) REFERENCES `user`(user_id),
    FOREIGN KEY (res_id) REFERENCES restaurant(res_id)
);

/* Tim 5 nguoi like nha hang nhieu nhat */
SELECT
	lr.user_id,
    u.full_name,
    COUNT(*) AS count_like
FROM like_res lr
INNER JOIN `user` u ON lr.user_id = u.user_id
GROUP BY lr.user_id, u.full_name
ORDER BY COUNT(*) DESC
LIMIT 5;

/* Tim 2 nha hang co luot like nhieu nhat */
SELECT
	lr.res_id,
    r.res_name,
    COUNT(*) AS count_like
FROM like_res lr
INNER JOIN restaurant r ON lr.res_id = r.res_id
GROUP BY lr.res_id, r.res_name
ORDER BY COUNT(*) DESC
LIMIT 2;

/* Tim nguoi da dat hang nhieu nhat */
SELECT MAX(count_order)
FROM (
	SELECT
		`o`.`code`,
		COUNT(*) AS count_order
	FROM `user` u
	INNER JOIN `order` o ON u.user_id = o.user_id
	GROUP BY `o`.`code`) tbl_count;

/* Tim nguoi dung khong hoat dong */
SELECT u.user_id, u.full_name
FROM `user` u
LEFT JOIN like_res lr ON lr.user_id = u.user_id
LEFT JOIN rate_res rr ON rr.user_id = u.user_id
LEFT JOIN `order` o ON o.user_id = u.user_id
WHERE lr.user_id IS NULL AND rr.user_id IS NULL AND o.user_id IS NULL;

/* Tinh trung binh 1 food co bao nhieu subfood */
SELECT AVG(count_sub)
FROM (SELECT f.food_id, COUNT(sf.sub_id) AS count_sub
	FROM food f
	INNER JOIN sub_food sf ON sf.food_id = f.food_id
	GROUP BY f.food_id) tbl_count_sub;
